const cookieParser = require('cookie-parser');
const createError = require('http-errors');
const session = require('express-session');
const mongoose = require('mongoose');
const express = require('express');
const config = require('config');
const logger = require('morgan');
const path = require('path');


const authMiddleware = require('./middlewares/auth');
const usersRouter = require('./routes/users');
const indexRouter = require('./routes/index');
const ownersRouter = require('./routes/owners');
const bankRouter = require('./routes/bank');
// const hotelsRouter = require('./routes/hotels');


const app = express();

mongoose.connect("mongodb://5.253.27.85:27017/development", {useNewUrlParser: true})
    .then(() => {console.log("Connected to database.");})
    .catch(err => {console.log("Couldn't connect to database!", err);});


app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.use(session({
    key: 'user',
    secret: "sEssi0nSecret",
    resave: false,
    saveUninitialized: false,
    cookie: {
        expires: 600000
    }
}));


app.use(express.static(path.join(__dirname, 'public')));

app.use(authMiddleware);

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/owners', ownersRouter);
app.use('/bank', bankRouter);
// app.use('./hotels', hotelsRouter);


app.use((req, res, next) => {
    next(createError(404));
});


app.use(function(err, req, res, next) {
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    res.status(err.status || 500);
    res.render('error');
});


module.exports = app;
