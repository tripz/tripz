const {AbstractUser} = require('../models/abstract_models/AbstractUser');


async function authUser(req, res, next) {
    // if (error)
    //     return res.status(400).send(error.details[0].message);
    try {
        // user = await
        // console.log("In authUser method " + JSON.stringify(req.body));
        req.user = await AbstractUser.findUser(req);
        // console.log("User created " + JSON.stringify(req.user));
        next()
    } catch(error){
        console.log(error);
        res.redirect('/users/login');
    }
}
module.exports = authUser;
