const _ = require('lodash');
const {Hotel, validateHotel} = require("../database_models/hotel");
const {Room, validateRoom} = require("../database_models/room");

class AbstractHotel{
    static async createNewModel(params, res, ownerModel){

        const {error} = validateHotel(params);
        if (error) {
            // console.log();
            return res.status(400).send(error.details[0].message);
        }
        params.owner = ownerModel._id;
        let hotel = await new Hotel(params);
        hotel.save(function (error, doc) {
            if (error){
                console.log(error + " from createNewModel in AbstractHotel");
                return res.status(400).send("Something got wrong, Maybe hotel already exists.")
            }
            ownerModel.hotels.push(doc._id);
            ownerModel.save(function (error, own) {
                if (error){
                    console.log(error + " from createNewModel in AbstractHotel");
                    return res.status(400).send(error)
                }
                res.send("New hotel added to your inventory");
            })
        })
    }

    constructor(model){
        this.model = model
    }

    async editHotel(params, res) {
        let thhotel = this;
        Hotel.updateOne({'_id': this.model._id}, params, function (error, hotel) {
            if(error){
                res.status(400).send(error)
            }
            // console.log(hotel);
            thhotel.model = hotel;
            return res.send("Hotel fields updated properly.")
        })
    }

    async editRoom(params, res){
        let thhotel = this;
        // let roomId = params._id;
        // delete params._id;
        console.log(params);
        Room.updateOne({'_id': params._id}, params, async function (error, room) {
            if(error){
                res.status(400).send(error)
            }
            // console.log(hotel);

            // let throom = await thhotel.model.rooms.filter((room) => {room._id == params._id})[0];
            // if(!throom) {
            //     console.log(thhotel.model.rooms + " " + params._id);
            //     return res.status(400).send('Not a valid room');
            // }
            // thhotel.pop(throom);
            // thhotel.model.rooms.push(room);
            // console.log("thhotel this " + thhotel.model.rooms);
            return res.send("Room fields updated properly.")

        })
    }

    async addRoom(params, res){
        params.hotel = this.model._id;
        let hotel = this.model;
        // console.log(params);
        if (!validateRoom(params))
            return res.status(400).send("Not valid parameters.");
        let room = await new Room(params);
        room.save(function (error, doc) {
            if (error){
                return res.status(400).send(error)
            }
            hotel.rooms.push(doc);
            hotel.save(function (error, doc) {
                if (error){
                    return res.status(400).send(error)
                }
                res.send("New room added to specified hotel.")
            })
        })
    }

}

exports.AbstractHotel = AbstractHotel;