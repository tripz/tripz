const _ = require('lodash');
const {OwnerInfo, validateOwnerRegister} = require("../database_models/ownerInformation");
const {AbstractHotel} = require("../abstract_models/AbstractHotel");

class AbstractOwnerState{
    static async findState(model){
        if(model && model.ownerInfo) {
            // console.log("OwnerState creation: " + model);
            let ownerinfo = await OwnerInfo.findOne({_id: model.ownerInfo}).populate({
                path: 'hotels',
                populate: { path: 'rooms' }
            });
            // console.log("did you see hotels? " + ownerinfo);
            return new Promoted(ownerinfo);
        }
        return new Default()
    }

    static async createNewModel(req, res, userModel){
        var ownerin = new OwnerInfo({phone: req.body.phone, personalID: req.body.personalID, user: userModel._id});
        await ownerin.save(function (error, doc) {
            if (error){
                return res.status(400).send(error)
            }
            res.send("Promoted");
            userModel.ownerInfo = doc;
            userModel.save()

        });
        ownerin.hotels = [];
        return new Promoted(ownerin)
    }

    promote(req, res){}

    async addHotel(req, res){}

    async addRoom(req, res){}

    async editHotel(req, res){}

    // promote(req, res, userModel){}

}

class Promoted extends AbstractOwnerState{

    constructor(ownerInfo){
        super();
        this.model = ownerInfo;
        this.hotels = [];
        for(let index = 0 ; index < this.model.hotels.length; index++){
            this.hotels.push(new AbstractHotel(this.model.hotels[index]));
        }
        console.log("New Promoted ownerState");
        // console.log("Hotels: " + this.hotels)
    }

    promote(req, res, userModel){
        res.status(400).send("Already promoted");
    }

    async addHotel(req, res){
        console.log("Promoted OwnerState addHotel");
        // console.log(req.body);
        let params = _.pick(req.body, ["name", "city", "address", "phone", "services.wifi", "services.pool",
            "services.playGround", "services.coffee", "services.taxi"]);
        // params.owner=this.model._id;
        // console.log(params);
        AbstractHotel.createNewModel(params, res, this.model);
        // let flag = await AbstractHotel.checkNewHotel(params, res, this.model);
        // console.log(flag);
        // res.send("Done");
    }
    async getHotels(req, res){
        res.render('my-hotels', {
            hotels: this.hotels
          });
    }

    async addRoom(req, res){
        let hotel = this.hotels.filter((hotel) => {return hotel.model._id == req.params.hotelId})[0];
        if(!hotel) {
            return res.status(400).send('Not a valid hotel');
        }
        // console.log(hotel);
        let params = _.pick(req.body, ["type", "isSuit", "price"]);
        hotel.addRoom(params, res)
    }

    async editHotel(req, res){

        let hotel = this.hotels.filter((hotel) => {return hotel.model._id == req.params.hotelId})[0];
        if(!hotel) {
            return res.status(400).send('Not a valid hotel');
        }

        let params = _.pick(req.body, ["name", "city", "address", "phone", "services.wifi", "services.pool",
            "services.playGround", "services.coffee", "services.taxi"]);

        return hotel.editHotel(params, res);

    }

    async editRoom(req, res){

        let hotel = this.hotels.filter((hotel) => {return hotel.model._id == req.params.hotelId})[0];
        if(!hotel) {
            return res.status(400).send('Not a valid hotel');
        }

        let params = _.pick(req.body, ["isSuit", "type", "price"]);
        console.log(params);
        params._id = req.params.roomId;

        return hotel.editRoom(params, res)



    }
}

class Default extends AbstractOwnerState{

    constructor(){
        super(null);
        console.log("New Default ownerState");
    }
    async addRoom(req, res){
        res.status(404).send("Page not found")
    }
    async addHotel(req, res){
        res.status(404).send("Page not found")
    }
    async promote(req, res, userModel){
        const {error} = validateOwnerRegister(req.body);
        if (error) {
            res.status(400).send(error.details[0].message);
            return this
        }
        // res.send("Promoted");
        return AbstractOwnerState.createNewModel(req, res, userModel)
    }
    async getHotels(req, res){
        res.status(404).send("Page not found")
    }
    async editHotel(req, res){
        res.status(404).send("Page not found")
    }
    async editRoom(req, res){
        res.status(404).send("Page not found")
    }
}

exports.AbstractOwnerState = AbstractOwnerState;