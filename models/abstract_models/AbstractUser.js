const _ = require('lodash');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const {User, validateRegister, validateLogin} = require("../database_models/user");
const {AbstractOwnerState} = require("../abstract_models/AbstractOwnerState");
const {Room, validateRoom} = require("../database_models/room");
const {Token} = require('../database_models/token');
const nodemailer = require("nodemailer");
const crypto = require('crypto');
const {Hotel, validateHotel} = require("../database_models/hotel");

class AbstractUser{

    static async findUserWithSession(req) {
        const token = req.session.user;
        try {
            req.user = jwt.verify(token, "jwtPrivateKey");;
            let user = await User.findOne({_id: req.user._id});
            if (!user) {
                let ownerState = await AbstractOwnerState.findState(null);
                // console.log(ownerState);
                return new NullUser(user, ownerState);
            }
            return AuthenticatedUser.createAuthUser(user);
        } catch (error) {
            let ownerState = await AbstractOwnerState.findState(null);
            return new NullUser(null, ownerState);
        }
    }

    static async findUserWithEmail(req){
        // console.log("in find"+ req.body);
        let usr = await User.findOne({email: req.body.email});
        // console.log(usr);
        return await usr
    }

    static async findUser(req){
        if (req.session.user)
            return this.findUserWithSession(req);

        let ownerState = await AbstractOwnerState.findState(null);
        return new NullUser(null, ownerState);
    }

    constructor(model, ownerState){
        this.model = model;
        this.ownerState = ownerState
        console.log("Abstract User created");
    }

    async postProfile(req, res){ }

    postRegister(req, res){}

    postLogin(req, res){}

    postLogout(req, res){}

    async postPromoteToOwner(req, res){}

    getRegister(req, res){}

    getLogin(req, res){}

    getProfile(req, res){}

    getHotels(req, res){
        // console.log(req);
        let search = _.pick(req.query, ['city', 'name', 'price.min', 'price.max']);
        if (search.price) {
            search.price.min = search.price.min ? search.price.min : 0;
            search.price.max = search.price.max ? search.price.max : 10000000;
        }else{
            search.price = {min: 0, max: 10000000};
        }
        console.log(search);
        Hotel.find(
            {
                "name": search.name? {"$regex": ".*" + search.name + ".*"}:{"$regex": ".*"},
                "city": search.city || {"$regex": ".*"},
            }).populate({'path': 'rooms'}).exec(function (error, doc) {
            let results = [];
            for(let d = 0 ; d < doc.length ; d++){
                    for(let r = 0 ; r < doc[d].rooms.length ; r++){
                        console.log(doc[d].rooms[r]);
                        if(doc[d].rooms[r].price >= search.price.min && doc[d].rooms[r].price <= search.price.max)
                        {
                            results.push(doc[d]);
                            break
                        }
                    }
                }
                console.log(results);

                    return res.render('search', {
                        hotels: results
                })
            });
    }

    async getHotel(req, res){
        Hotel.find({ _id: req.params.hotelId }, async function (error, doc) {
            let rooms = [];
            for (let i = 0; i < doc[0].rooms.length; i++){
                const room = await Room.find({_id: doc[0].rooms[i]});
                rooms.push(room);
            }
            console.log(rooms);
            return res.render('hotel', {
                hotel: doc[0], 
                rooms: rooms
              });
        })
    }

    // addHotel(req, res){
    //     this.ownerState.addHotel(req, res)
    // }
    //
    // addRoom(req, res){
    //     this.ownerState.addRoom(req, res)
    // }
}

class AuthenticatedUser extends AbstractUser{

    static async createAuthUser(userModel){

        let ownerState = await AbstractOwnerState.findState(userModel);
        // console.log(JSON.stringify(ownerState));

        return await new AuthenticatedUser(userModel, ownerState);
    }

     constructor(model, ownerState){
        super(model, ownerState);
        console.log("Authenticated User created");
    }

    async postProfile(req, res){
        if (req.body.repassword != req.body.password)
            return res.redirect("/users/profile");
        if (req.body.name)
            this.model.name = req.body.name;
        if (req.body.password) {
            const salt = await bcrypt.genSalt(10);
            this.model.password = await bcrypt.hash(req.body.password, salt);
        }
        await this.model.save();
        res.redirect("/users/profile");
    }

    postRegister(req, res){
        return res.status(400).send('User already registered.');
    }

    postLogin(req, res){
        // console.log(JSON.stringify(req.body));
        return res.redirect("/users/profile")
    }

    postLogout(req, res){
        console.log("Real user logging out");
        req.session.user = null;
        res.redirect('/users/login');
    }

    async postPromoteToOwner(req, res){
        this.ownerState = this.ownerState.promote(req, res, this.model);
    }

    getRegister(req, res){
        return res.redirect(307, '/users/profile');
    }

    getLogin(req, res){
        return res.redirect(307, '/users/profile');
    }

    getProfile(req, res){
        return res.render('profile', {currentUser: this.model})
    }

    getConfirmation(req, res){
        return res.status(400).send('First you need to logout.')
    }
}

class NullUser extends AbstractUser{

    constructor(model, ownerState){
        super(model, ownerState);

        console.log("Null user created");
    }

    async postRegister(req, res){
        const {error} = validateRegister(req.body);
        if (error)
            return res.status(400).send(error.details[0].message);
        let usr = await AbstractUser.findUserWithEmail(req);
        if (usr){
            return res.status(400).send("User already exists");
        }
        let user = await new User(_.pick(req.body, ['name', 'email', 'password']));
        const salt = await bcrypt.genSalt(10);
        user.password = await bcrypt.hash(user.password, salt);
        await user.save();

        var token = new Token({ _userId: user._id, token: crypto.randomBytes(16).toString('hex') });
        token.save(async function (err) {
            if (err) { return res.status(500).send({ msg: err.message }); }

            // Send the email
            let testAccount = await nodemailer.createTestAccount();

            // create reusable transporter object using the default SMTP transport
            let transporter = nodemailer.createTransport({
                // host: "smpt.gmail.com",
                // port: 587,
                // secure: false, // true for 465, false for other ports
                service: 'gmail',
                auth: {
                    user: "tripz.agile@gmail.com", // generated ethereal user
                    pass: "tripZpass" // generated ethereal password
                }
            });
            // console.log(transporter);
            var mailOptions = { from: 'no-reply@yourwebapplication.com', to: user.email, subject: 'Account Verification Token', text: 'Hello,\n\n' + 'Please verify your account by clicking the link: \nhttp:\/\/' + req.headers.host + '\/users\/confirmation\/' + token.token + '.\n' };
            transporter.sendMail(mailOptions, function (err) {
                if (err) { return res.status(500).send({ msg: err.message }); }
                res.status(200).send('A verification email has been sent to ' + user.email + '.');
            });
        });

        // res.redirect("/users/login");
    }

    postProfile(req, res){
        return res.redirect("/users/login");
    }

    async postLogin(req, res){
        const {error} = validateLogin(req.body);
        if (error) {
            return res.status(400).send(error.details[0].message);
        }
        let usr = await AbstractUser.findUserWithEmail(req);
        if(!usr){
            return res.status(400).send('Email or password incorrect');
        }
        const validPassword = await bcrypt.compare(req.body.password, usr.password);
        if (!validPassword) {
            return res.status(400).send('Invalid email or password!');
        }
        // console.log(usr);
        if (!usr.emailValidated) {
            return res.status(401).send({ type: 'not-verified', msg: 'Your account has not been verified.' });
        }
        req.session.user = jwt.sign({_id: usr._id}, 'jwtPrivateKey', {expiresIn: 1440});;
        res.redirect('/users/profile');
    }


    postLogout(req, res){
        return res.status(400).redirect('/users/login');
    }

    postPromoteToOwner(req, res){
        return res.status(400).send('You should login first');
    }

    getProfile(req, res){
        return res.redirect("/users/login");
    }

    getConfirmation(req, res) {

        Token.findOne({ token: req.params.tokenId }, function (err, token) {
            if (!token) return res.status(400).send({ type: 'not-verified', msg: 'We were unable to find a valid token. Your token my have expired.' });

            // If we found a token, find a matching user
            User.findOne({ _id: token._userId }, function (err, user) {
                if (!user) return res.status(400).send({ msg: 'We were unable to find a user for this token.' });
                if (user.isVerified) return res.status(400).send({ type: 'already-verified', msg: 'This user has already been verified.' });

                // Verify and save the user
                user.emailValidated = true;
                user.save(function (err) {
                    if (err) { return res.status(500).send({ msg: err.message }); }
                    res.status(200).send("The account has been verified. Please log in.");
                });
            });
        });
    }

    getLogin(req, res){
        return res.render('login', {})
    }

    getRegister(req, res){
        return res.render('register', {});
    }

}


exports.AbstractUser = AbstractUser;