const mongoose = require('mongoose');
const Joi = require('joi');

const DateRange = mongoose.model('DateRange', new mongoose.Schema({
    start: {
        type: Date,
        required: true
    },
    end: {
        type: Date,
        required: true
    }
}));

function validateHotel(DateRange) {
    const schema = {

    };

    return Joi.validate(DateRange, schema);
}

exports.DateRange = DateRange;
exports.validate = validateHotel;
