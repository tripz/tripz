const mongoose = require('mongoose');
const Joi = require('joi');
const Schema = mongoose.Schema;

const Hotel = mongoose.model('Hotel', new mongoose.Schema({
    name: {
        type: String,
        required: true,
        minlength: 5,
        maxlength: 50
    },
    // wifi, estakhr, zamine bazi,khoshkshuyi, coffee shop, taxi service
    services: {
        wifi: {
            type: Boolean,
            default: false
        },
        pool: {
            type: Boolean,
            default: false,
        },
        playGround: {
            type: Boolean,
            default: false
        },
        coffee: {
            type: Boolean,
            default: false
        },
        taxi: {
            type: Boolean,
            default: false
        }
    },
    city: {
        type: String,
        required: true,
        minlength: 5,
        maxlength: 50,
    },
    address: {
        type: String,
        required: true,
        minlength: 5,
        maxlength: 255,
        unique: true
    },
    phone: {
        type: String,
        required: true,
        maxlength: 12,
        unique: true
    },
    rooms: [
        {
            type: Schema.Types.ObjectId,
            ref: "Room"
        }
    ],
    // suits: [
    //     {
    //         type: Schema.Types.ObjectId,
    //         ref: "Room"
    //     }
    // ],
    owner: {
        type: Schema.Types.ObjectId,
        ref: "OwnerInformation",
        required: true
    }
}));

function validateHotel(Hotel) {
    const schema = {
        name: Joi.string().min(5).max(50).required(),
        city: Joi.string().min(5).max(50).required(),
        address: Joi.string().min(5).max(255).required(),
        // rooms: Joi.number().min(1).max(5).required(),
        // services: {
            'services.wifi': Joi.boolean(),
            'services.pool': Joi.boolean(),
            'services.playGround': Joi.boolean(),
            'services.coffee': Joi.boolean(),
            'services.taxi': Joi.boolean(),

        // }.required,
        phone: Joi.string().max(10).required(),
        // owner: Joi.validate
    };

    return Joi.validate(Hotel, schema);
}

exports.Hotel = Hotel;
exports.validateHotel = validateHotel;
