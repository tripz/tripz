const mongoose = require('mongoose');
const Joi = require('joi');
const Schema = mongoose.Schema;

const OwnerInformation = mongoose.model('OwnerInformation', new mongoose.Schema({
    phone: {
        type: String,
        required: true,
        maxlength: 12,
        unique: true
    },
    personalID: {
        type: String,
        required: true,
        minlength: 10,
        maxlength: 10,
        unique: true
    },
    ownerValidated: {
        type: Boolean,
        default: false,
        required: true
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: "User",
        required: true
    },
    hotels: [
        {
            type: Schema.Types.ObjectId,
            ref: "Hotel"
        }
    ]
}));

function validateRegister(hotelOwner) {
    const schema = {
        // name: Joi.string().min(5).max(50).required(),
        // email: Joi.string().min(5).max(255).required().email(),
        // password: Joi.string().min(8).max(255).required(),
        personalID: Joi.string().min(10).max(10).required(),
        phone: Joi.string().min(8).required(),
        // repassword: Joi.string().min(8).max(255).required(),
    };

    return Joi.validate(hotelOwner, schema);
}

exports.OwnerInfo = OwnerInformation;
exports.validateOwnerRegister = validateRegister;
