const mongoose = require('mongoose');
const Joi = require('joi');
const Schema = mongoose.Schema;

const Room = mongoose.model('Room', new mongoose.Schema({
    isSuit: {
        type: Boolean,
        required: true
    },
    price: {
        type: Number,
        required: true
    },
    type: {
        type: String,
        required: true
    },
    reservedDates: [
        {
            type: Schema.Types.ObjectId,
            ref: "DateRange",
        }
    ],
    hotel: {
        type: Schema.Types.ObjectId,
        ref: "Hotel"
    }
}));

function validateRoom(Room) {
    const schema = {
        // name: Joi.string().min(5).max(50).required(),
        isSuit: Joi.boolean().required(),
        type: Joi.string().min(3).required(),
        price: Joi.required()
        // rooms: Joi.number().min(1).max(5).required(),
        // phone: Joi.string().max(10).required(),

    };

    return Joi.validate(Room, schema);
}

exports.Room = Room;
exports.validateRoom = validateRoom;
