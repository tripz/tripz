const mongoose = require('mongoose');
const Joi = require('joi');
const Schema = mongoose.Schema;


const User = mongoose.model('User', new mongoose.Schema({
    name: {
        type: String,
        required: true,
        maxlength: 50
    },
    email: {
        type: String,
        required: true,
        maxlength: 255,
        unique: true
    },
    password: {
        type: String,
        required: true,
        maxlength: 1024
    },
    emailValidated: {
        type: Boolean,
        required: true,
        default: false,
    },
    ownerInfo: {
        type: Schema.Types.ObjectId,
        ref: 'OwnerInformation'
    }
}));

function validateRegister(user) {
    if(user.password != user.password)
        return false;
    const schema = {
        name: Joi.string().max(50).required(),
        email: Joi.string().max(255).required().email(),
        password: Joi.string().min(8).max(255).required(),
        repassword: Joi.string().min(8).max(255).required()
    };

    return Joi.validate(user, schema);
}



function validateLogin(req){
    const schema = {
        email: Joi.string().max(255).required().email(),
        password: Joi.string().min(8).max(255).required()
    };

    return Joi.validate(req, schema);
}



exports.User = User;
exports.validateRegister = validateRegister;
exports.validateLogin = validateLogin;
