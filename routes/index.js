const {Hotel, validate} = require('../models/database_models/hotel');
const express = require('express');
const router = express.Router();

router.get('/', async (req, res, next) => {
  const hotels = await Hotel.find();
  res.render('index', {
    hotels: hotels
  });
});


module.exports = router;
