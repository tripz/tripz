const {Owner, validate} = require('../models/database_models/ownerInformation');
const auth = require('../middlewares/auth');
const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
const express = require('express');
// const config = require('config');
const bcrypt = require('bcrypt');
const _ = require('lodash');
const Joi = require('joi');

const router = express.Router();


router.post('/register', async (req, res) => {
    const {error} = validate(req.body);
    if (error)
        return res.status(400).send(error.details[0].message);
    if (req.body.repassword != req.body.password)
        return res.redirect("/owners/register");
    try {
        let owner = await Owner.findOne({email: req.body.email});
        if (owner) return res.status(400).send('Owner already registered.');
        console.log(req.body);
        owner = new Owner(_.pick(req.body, ['name', 'email', 'personalID', 'phone', 'password']));
        const salt = await bcrypt.genSalt(10);
        owner.password = await bcrypt.hash(owner.password, salt);

        await owner.save();

        res.redirect("/owners/profile");
    } catch(error){
        console.log(error + ' in???')
    }
});

router.post('/login', async (req, res) => {
    const {error} = validateLogin(req.body);
    if (error)
        return res.status(400).send(error.details[0].message);
    try {
        let owner = await Owner.findOne({email: req.body.email});
        if (!owner) return res.status(400).send('Invalid email or password!');

        const validPassword = await bcrypt.compare(req.body.password, owner.password);
        if (!validPassword) return res.status(400).send('Invalid email or password!');
        const token = jwt.sign({_id: owner._id}, 'jwtPrivateKey', {expiresIn: 1440});
        req.session.owner = token;
        res.redirect('/owners/profile');
    }catch(error){
        console.log("Post Login");
        console.log(error)
    }
});

router.get('/logout', async (req, res) => {
    if (await isLoggedIn(req))
        req.session.owner = null;
    res.redirect('/owners/login');
});

router.get('/register', async (req, res) => {
    console.log("inja");
    if (await isLoggedIn(req)){
        res.redirect(307, '/owners/profile');
        return;
    }
    console.log("Here");
    res.render('register-placeowner', {})
});

router.get('/login', async (req, res) => {
    if (!await isLoggedIn(req)){
        res.render('login-placeowner', {});
        return
    }
});

router.get('/profile', async function (req, res) {
    if (!await isLoggedIn(req)){
        res.redirect('/owners/login')
    }
    res.render('profile-placeowner', {currentOwner: await isLoggedIn(req)})
});

router.post('/profile', async (req, res) => {
    let user = await isLoggedIn(req);
    // console.log(user + '1');
    if (!user)
        return res.redirect("/owners/login");
    if (req.body.repassword != req.body.password)
        return res.redirect("/owners/profile");
    if (req.body.name)
        user.name = req.body.name;
    if (req.body.password) {
        const salt = await bcrypt.genSalt(10);
        user.password = await bcrypt.hash(req.body.password, salt);
    }
    try {
        await user.save();
    }catch(error){
        console.log(error)
    }
    res.redirect("/owners/profile");
});

function validateLogin(req){
    const schema = {
        email: Joi.string().max(255).required().email(),
        password: Joi.string().min(8).max(255).required()
    };

    return Joi.validate(req, schema);
}

async function isLoggedIn(req){
    const token = req.session.owner;
    console.log(req.session);
    if (!token)
        return false;

    try {
        const decoded = jwt.verify(token, "jwtPrivateKey");
        req.owner = decoded;
        let owner = await Owner.findOne({_id: req.owner._id});
        if (!owner)
            return false;
        return owner;
    } catch (error) {
        return false;
    }
}

module.exports = router;
