const express = require('express');

const router = express.Router();


router.post('/register', async (req, res) => {
    req.user.postRegister(req, res);
});

router.post('/login', async (req, res) => {
    // console.log("In routing " + JSON.stringify(req.user));
    req.user.postLogin(req, res);
});

router.post('/profile', async (req, res) => {
    req.user.postProfile(req, res);
});

router.post('/owner_promotion', (req, res) => {
    // console.log("going to promote");
    req.user.postPromoteToOwner(req, res);
});

router.post('/add_hotel', async (req, res) => {
    req.user.ownerState.addHotel(req, res);
});

router.get('/get_my_hotels', async (req, res) => {
    req.user.ownerState.getHotels(req, res)
});

router.post('/:hotelId/add_room', async (req, res) => {
    req.user.ownerState.addRoom(req, res)
    // res.send(req.params)
});

router.post('/:hotelId', async (req, res) => {
    req.user.ownerState.editHotel(req, res)
});

router.post('/:hotelId/:roomId', async (req, res) => {
    // console.log(req.body);
    // console.log(req);
    req.user.ownerState.editRoom(req, res)
});

router.get('/logout', async (req, res) => {
    req.user.postLogout(req, res);
});


router.get('/register', async (req, res) => {
    req.user.getRegister(req, res);
});

router.get('/login', async (req, res) => {
    req.user.getLogin(req, res);
});

router.get('/profile', async (req, res) => {
    req.user.getProfile(req, res);
});

router.get('/confirmation/:tokenId', (req, res) => {
    // console.log(req.body);
    req.user.getConfirmation(req, res);
    // res.send(req.body)
});

router.get('/hotel/:hotelId', (req, res) => {
    req.user.getHotel(req, res)
})

router.get('/get_hotels', (req, res) => {
    console.log("inja");
    req.user.getHotels(req, res)
})

// router.post('/resendToken', (req, res) => {
//     User.findOne({ email: req.body.email }, function (err, user) {
//         if (!user) return res.status(400).send({ msg: 'We were unable to find a user with that email.' });
//         if (user.isVerified) return res.status(400).send({ msg: 'This account has already been verified. Please log in.' });
//
//         // Create a verification token, save it, and send email
//         var token = new Token({ _userId: user._id, token: crypto.randomBytes(16).toString('hex') });
//
//         // Save the token
//         token.save(function (err) {
//             if (err) { return res.status(500).send({ msg: err.message }); }
//
//             // Send the email
//             var transporter = nodemailer.createTransport({ service: 'Sendgrid', auth: { user: process.env.SENDGRID_USERNAME, pass: process.env.SENDGRID_PASSWORD } });
//             var mailOptions = { from: 'no-reply@codemoto.io', to: user.email, subject: 'Account Verification Token', text: 'Hello,\n\n' + 'Please verify your account by clicking the link: \nhttp:\/\/' + req.headers.host + '\/confirmation\/' + token.token + '.\n' };
//             transporter.sendMail(mailOptions, function (err) {
//                 if (err) { return res.status(500).send({ msg: err.message }); }
//                 res.status(200).send('A verification email has been sent to ' + user.email + '.');
//             });
//         });
//
//     });
// });

module.exports = router;
